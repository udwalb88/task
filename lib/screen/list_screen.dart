import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:refactor_task/screen/list_controller.dart';
import 'package:refactor_task/widget/list_item.dart';

class ListScreen extends GetView<ListController> {
  static const routeName = '/LoginScreen';

  const ListScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Scaffold(
        backgroundColor:
            controller.isDarkMode.value ? Colors.black : Colors.white,
        appBar: AppBar(
          backgroundColor:
              controller.isDarkMode.value ? Colors.black : Colors.white,
          title: Text(
            'Refactor Task App',
            style: TextStyle(color: controller.color),
          ),
          actions: [
            Obx(
              () => InkWell(
                onTap: controller.onTapDarkMode,
                child: Container(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 6, vertical: 2),
                    decoration: BoxDecoration(
                      border: Border.all(color: controller.color),
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: Text(
                      controller.isDarkMode.value ? 'dark mode' : 'light mode',
                      style: TextStyle(color: controller.color),
                    )
                    // CupertinoSwitch(
                    //   value: controller.isDarkMode.value,
                    //   activeColor: Colors.black,
                    //   trackColor: Colors.white,
                    //   onChanged: (bool? value) {
                    //     controller.isDarkMode.value = value ?? false;
                    //   },
                    // ),
                    ),
              ),
            ),
          ],
        ),
        body: Obx(
          () => controller.isLoading.value
              ? const SizedBox(
                  height: 200,
                  child: Center(
                    child: CircularProgressIndicator(
                      valueColor:
                          AlwaysStoppedAnimation(Colors.deepPurpleAccent),
                    ),
                  ),
                )
              : Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 24, vertical: 12),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Data list',
                            style: TextStyle(
                                fontSize: 18, color: controller.color),
                          ),
                          const Spacer(),
                          Padding(
                            padding: const EdgeInsets.only(right: 8),
                            child: Text(
                              controller.isGrid.value
                                  ? 'view list'
                                  : 'view grid',
                              style: TextStyle(color: controller.color),
                            ),
                          ),
                          InkWell(
                            onTap: controller.onTapGrid,
                            child: controller.isGrid.value
                                ? Icon(
                                    Icons.dehaze,
                                    color: controller.color,
                                  )
                                : Icon(
                                    Icons.grid_view_outlined,
                                    color: controller.color,
                                  ),
                          ),
                        ],
                      ),
                    ),
                    controller.isGrid.value
                        ? Expanded(
                            child: GridView.builder(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 24),
                              gridDelegate:
                                  const SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 2,
                                crossAxisSpacing: 16,
                                mainAxisExtent: 280,
                              ),
                              itemCount: controller.itemList.length,
                              itemBuilder: (_, index) {
                                final item = controller.itemList[index];
                                return ListItem(
                                  listItem: item,
                                  color: controller.color,
                                );
                              },
                            ),
                          )
                        : Expanded(
                            child: ListView.builder(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 24),
                              shrinkWrap: true,
                              itemCount: controller.itemList.length,
                              itemBuilder: (context, index) {
                                final item = controller.itemList[index];
                                return ListItem(
                                  listItem: item,
                                  color: controller.color,
                                );
                              },
                            ),
                          ),
                  ],
                ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: controller.getData,
          tooltip: 'Fetch Data',
          backgroundColor: Colors.deepPurpleAccent,
          foregroundColor: Colors.white,
          child: const Icon(Icons.refresh),
        ),
      ),
    );
  }
}
