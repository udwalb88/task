import 'package:get/get.dart';
import 'package:refactor_task/screen/list_controller.dart';

class ListBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ListController());
  }
}
