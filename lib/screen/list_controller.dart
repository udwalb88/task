import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:g_json/g_json.dart';
import 'package:http/http.dart' as http;
import 'package:get/get.dart';
import 'package:refactor_task/model/model.dart';

class ListController extends GetxController {
  final itemList = <ListModel>[].obs;
  final isLoading = false.obs;
  final isGrid = false.obs;
  final isDarkMode = false.obs;

//controller initialize hiigdeh ued open api-g shuud duudna.
  @override
  void onInit() {
    super.onInit();
    getData();
  }

//grid view haruulah method
  onTapGrid() {
    isGrid.value = !isGrid.value;
  }

//dark view haruulah method
  onTapDarkMode() {
    isDarkMode.value = !isDarkMode.value;
  }

  Color get color {
    return !isDarkMode.value ? Colors.black : Colors.white;
  }

  void getData() async {
    try {
      isLoading.value = true;
      String url = "https://jsonplaceholder.typicode.com/posts";
      http.Response res = await http.get(Uri.parse(url));
      if (res.statusCode == 200) {
//hive sudalj amjaagu uchir json ashiglan dataga hurvuulj avsan

        final myData = json.decode(res.body) as List<dynamic>;
        final jsonData = JSON(myData);
        isLoading.value = false;
        // final jsonData = JSON(json.decode(res.body));
        // final itemFromData = myData
        //     .map(
        //       (e) => ListModel.fromMap(e as Map<String, dynamic>),
        //     )
        //     .toList();
        // final itemFromJson =
        //     jsonData.listValue.map((item) => ListModel.fromJSON(item)).toList();

//ItemList model uusgen jsondata-g map-lana

        itemList.value =
            jsonData.listValue.map((item) => ListModel.fromJSON(item)).toList();
        print(itemList.length);
      }
    } catch (e) {
      debugPrint(e.toString());
    }
  }
}
