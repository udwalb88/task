import 'package:get/get.dart';
import 'package:refactor_task/screen/list_binding.dart';
import 'package:refactor_task/screen/list_screen.dart';

class RouteTest {
  static String get initialRoute {
    return ListScreen.routeName;
  }

  static List<GetPage> pages = [
    GetPage(
      name: ListScreen.routeName,
      page: () => const ListScreen(),
      binding: ListBinding(),
    ),
  ];
}
