import 'package:flutter/material.dart';
import 'package:refactor_task/model/model.dart';

class ListItem extends StatelessWidget {
  final ListModel listItem;
  final Color color;
  const ListItem({super.key, required this.listItem, required this.color});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(12),
      margin: const EdgeInsets.symmetric(vertical: 12),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.blueGrey.shade100),
        borderRadius: BorderRadius.circular(16),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text(
                'Id: ',
                style: TextStyle(fontSize: 16, color: color),
              ),
              Text(
                listItem.id.toString(),
                style: TextStyle(color: color),
              ),
            ],
          ),
          Text(
            'Title',
            style: TextStyle(fontSize: 16, color: color),
          ),
          Text(listItem.title, style: TextStyle(color: color)),
          Text(
            'Detail',
            style: TextStyle(fontSize: 16, color: color),
          ),
          Text(
            listItem.description,
            style: TextStyle(color: color),
            overflow: TextOverflow.ellipsis,
          ),
        ],
      ),
    );
  }
}
