import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:refactor_task/route.dart';

import 'main_controller.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // Get.lazyPut(() => BadgeController());
  // await MainConfig.setFirebaseConfig();
  // await MainConfig.setStoreConfig();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final controller = Get.put(MainController());
  MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      debugShowMaterialGrid: false,
      getPages: RouteTest.pages,
      initialRoute: RouteTest.initialRoute,
    );
  }
}
