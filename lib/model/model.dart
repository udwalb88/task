import 'package:g_json/g_json.dart';

class ListModel {
  final int id;
  final String title;
  final String description;

  ListModel.fromMap(Map<String, dynamic> map)
      : id = int.tryParse(map['id'] as String) ?? 0,
        title = map['title'] as String? ?? '',
        description = map['body'] as String? ?? '';

  ListModel.fromJSON(JSON json)
      : id = json['id'].integerValue,
        title = json['title'].stringValue,
        description = json['body'].stringValue;

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'gift': title,
      'body': description,
    };
  }
}
